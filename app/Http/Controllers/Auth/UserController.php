<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function show()
    {
        return Auth::check() ? response()->json(Auth::user()) : response()->json([], 401);
    }
}
