<?php


namespace App\Http\Controllers\Categories;


use App\Http\Controllers\Controller;
use App\Models\Category;

class CategoryController extends Controller
{
    public function index()
    {
        dd(1);
        $categories = Category::all();

        return view('categories.index', [
            'categories' => $categories
        ]);
    }

    public function show(Category $category)
    {
        return response()->json($category);
    }
}