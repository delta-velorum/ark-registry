<?php


namespace App\Http\Controllers\Categories;


use App\Http\Controllers\Controller;
use App\Http\Requests\Categories\StoreCategory;
use App\Http\Requests\Categories\UpdateCategory;
use App\Models\Category;
use App\Models\MergeRequest;
use Illuminate\Support\Facades\Auth;

class ManageCategoryController extends Controller
{
    public function store(StoreCategory $request)
    {
        $data = $request->validated();

        $category = Category::create($data);

        return $category->toJson();
    }

    public function update(UpdateCategory $request, Category $category)
    {
        $data = $request->validated();

        MergeRequest::create([
            'user_id' => Auth::id(),
            'owner_id' => $category->id,
            'model' => Category::class,
            'payload' => json_encode($data)
        ]);

        return redirect()->route('categories')->with([
            'success' => sprintf('Successfully created category "%s". Changes will be visible once a moderator has approved this edit.', $category->name)
        ]);
    }

    public function delete(Category $category)
    {
        $category->delete();

        return redirect()->route('categories')->with([
            'success' => sprintf('Successfully deleted category "%s".', $category->name)
        ]);
    }
}