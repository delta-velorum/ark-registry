<?php


namespace App\Http\Controllers\Items;


use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Item;

class ItemController extends Controller
{
    public function index()
    {
        $items = Item::with('dlc')->paginate(50);

        return view('items.index', [
            'items' => $items
        ]);
    }

    public function show(Category $category)
    {
        return view('categories.show', [
            'category' => $category
        ]);
    }
}