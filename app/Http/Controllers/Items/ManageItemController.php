<?php


namespace App\Http\Controllers\Items;


use App\Http\Controllers\Controller;
use App\Http\Requests\Categories\StoreCategory;
use App\Http\Requests\Categories\StoreItem;
use App\Http\Requests\Categories\UpdateCategory;
use App\Models\Category;
use App\Models\DLC;
use App\Models\Item;
use App\Models\ItemMetadata;
use App\Models\MergeRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ManageItemController extends Controller
{
    public function create()
    {
        $dlcs = DLC::all();

        return view('items.create', [
            'dlcs' => $dlcs
        ]);
    }

    public function store(StoreItem $request)
    {

        /*
         * ITEM CATEGORIES
         */

        $data = $request->validated();

        DB::beginTransaction();

        try {
            $data['image_url'] = $data['image_url']->store('uploads/', ['disk' => 'public']);

            $item = Item::create($data);

            if (array_key_exists('metadata', $data) && is_array($data['metadata'])) {
                foreach ($data['metadata'] as $metadata) {
                    ItemMetadata::create($metadata);
                }
            }

            DB::commit();

            return redirect()->route('items')->with([
                'success' => sprintf('Successfully added item "%s".', $item->name)
            ]);

        } catch (\Exception $exception) {
            dd($exception);
            DB::rollBack();
            return redirect()->back()->withInput();
        }
    }

    public function edit($category)
    {
        $category = Category::where('slug', $category)->firstOrFail();

        return view('categories.edit', [
            'category' => $category
        ]);
    }

    public function update(UpdateCategory $request, $category)
    {
        $category = Category::where('slug', $category)->firstOrFail();

        $data = $request->validated();

        MergeRequest::create([
            'user_id' => Auth::id(),
            'owner_id' => $category->id,
            'model' => Category::class,
            'payload' => json_encode($data)
        ]);

        return redirect()->route('categories')->with([
            'success' => sprintf('Successfully created category "%s". Changes will be visible once a moderator has approved this edit.', $category->name)
        ]);
    }

    public function delete($category)
    {
        $category = Category::where('slug', $category)->firstOrFail();

        $category->delete();

        return redirect()->route('categories')->with([
            'success' => sprintf('Successfully deleted category "%s".', $category->name)
        ]);
    }
}