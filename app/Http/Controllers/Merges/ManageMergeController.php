<?php


namespace App\Http\Controllers\Merges;


use App\Http\Controllers\Controller;
use App\Http\Requests\Categories\StoreCategory;
use App\Http\Requests\Categories\UpdateCategory;
use App\Models\Category;
use App\Models\Merge;
use App\Models\MergeRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ManageMergeController extends Controller
{
    public function merge(MergeRequest $merge)
    {

    }

    public function json(Request $request)
    {
        $requests = MergeRequest::with('user', 'merge')->paginate($request->query('limit', 50));

        return $requests->toJson();
    }

    public function html(Request $request)
    {
        $requests = MergeRequest::with('user', 'merge')->paginate($request->query('limit', 50));

        return view('snippets.dashboard.merges', [
            'requests' => $requests
        ]);
    }

    public function approve($id)
    {
        $request = MergeRequest::with('user', 'merge', 'merge.moderator')->findOrFail($id);
        $original = $request->model::find($request->owner_id);

        $json = $original->toJson();
        $original->update(json_decode($request->payload, true));

        $merge = Merge::create([
            'merge_request_id' => $request->id,
            'user_id' => Auth::id(),
            'owner_id' => $original->id,
            'from' => $json
        ]);

        return redirect()->route('merges.show', $id);
    }

    public function revert($id)
    {
        $request = MergeRequest::with('user', 'merge', 'merge.moderator')->findOrFail($id);
        $original = $request->model::find($request->owner_id);

        $previous = json_decode($request->merge->from, true);
        $original->update($previous);

        return redirect()->route('merges.show', $id);
    }

    public function delete($id)
    {
        $request = MergeRequest::findOrFail($id);

        $request->delete();

        return redirect()->route('merges.index');
    }
}