<?php


namespace App\Http\Controllers\Merges;


use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\MergeRequest;

class MergeController extends Controller
{
    public function index()
    {
        return view('merges.index');
    }

    public function show($id)
    {
        $request = MergeRequest::with('user', 'merge', 'merge.moderator')->findOrFail($id);
        $original = $request->model::find($request->owner_id); // load the merge target
        $request->payload = json_decode($request->payload); // columns of the new request
        $keys = array_keys($original->toArray()); // columns of the current item
        $from = !!$request->merge ? json_decode($request->merge->from) : null; // if the request was accepted, $from will be the old data

        return view('merges.show', [
            'request' => $request,
            'original' => $original,
            'keys' => $keys,
            'from' => $from
        ]);
    }
}