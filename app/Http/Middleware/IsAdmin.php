<?php


namespace App\Http\Middleware;

use Illuminate\Http\Request;
use Closure;
use Illuminate\Support\Facades\Auth;

class IsAdmin
{
    public function handle(Request $request, Closure $next)
    {
        if (Auth::user()->role !== 'admin') return redirect()->back()->with([
            'danger' => 'You must be an admin to do that.'
        ]);

        return $next($request);
    }
}