<?php


namespace App\Http\Middleware;

use Illuminate\Http\Request;
use Closure;
use Illuminate\Support\Facades\Auth;

class IsModerator
{
    public function handle(Request $request, Closure $next)
    {
        if (!in_array(Auth::user()->role, ['moderator', 'admin'])) return redirect()->route('home')->with([
            'danger' => 'You must be a moderator to do that.'
        ]);
        
        return $next($request);
    }
}