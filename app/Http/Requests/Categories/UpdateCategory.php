<?php

namespace App\Http\Requests\Categories;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class UpdateCategory extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => [
                'required',
                'string',
                'max:256'
            ],
            'slug' => [
                'required',
                Rule::unique('categories', 'slug')->ignore($this->route('category'), 'slug'),
                'regex:/[a-zA-Z-0-9-]+/',
                'min:3',
                'max:64'
            ],
            'description' => [
                'nullable',
                'string',
                'max:4000000'
            ]
        ];
    }
}
