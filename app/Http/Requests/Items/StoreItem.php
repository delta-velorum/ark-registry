<?php

namespace App\Http\Requests\Categories;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class StoreItem extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => [
                'required',
                'string',
                'max:256'
            ],
            'slug' => [
                'required',
                'unique:items,slug',
                'regex:/[a-zA-Z-0-9-]+/',
                'min:3',
                'max:64'
            ],
            'description' => [
                'required',
                'string',
                'max:4000000'
            ],
            'added_in' => [
                'required',
                'numeric',
                'min:1'
            ],
            'removed_in' => [
                'nullable',
                'numeric',
                'min:1'
            ],
            'stack_size' => [
                'required',
                'numeric',
                'min:1',
            ],
            'weight' => [
                'required',
                'numeric',
                'min:0.01',
            ],
            'image_url' => [
                'required',
                'image',
                'max:1000'
            ],
            'metadata.*.value' => [
                'required',
            ],
            'metadata.*.type' => [
                'required',
                'in:string,boolean,numeric',
            ],
            'metadata.*.name' => [
                'required',
            ],
            'dlc_id' => [
                'nullable',
                'numeric',
                Rule::exists('dlcs', 'id')
            ]
        ];
    }
}
