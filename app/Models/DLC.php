<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class DLC extends Model
{
    protected $table = 'dlcs';

    protected $fillable = [
        'name',
        'image',
        'slug',
        'description',
        'released_at'
    ];


}