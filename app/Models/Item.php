<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $table = 'items';

    protected $fillable = [
        'name',
        'slug',
        'description',
        'added_in',
        'removed_in',
        'weight',
        'stack_size',
        'image_url',
        'dlc_id'
    ];

    public function dlc()
    {
        return $this->belongsTo(DLC::class);
    }
}