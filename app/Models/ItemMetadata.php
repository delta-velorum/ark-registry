<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class ItemMetadata extends Model
{
    protected $table = 'item_metadata';

    protected $fillable = [
        'item_id',
        'name',
        'type',
        'value',
    ];

    public function getFormattedValueAttribute()
    {
        switch ($this->type) {
            case 'string':
                return $this->value;
            case 'numeric':
                return floatval($this->value);
            case 'boolean':
                return $this->value === 'true';
        }

        return $this->value;
    }
}