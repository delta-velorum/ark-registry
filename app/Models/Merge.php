<?php


namespace App\Models;


use App\User;
use Illuminate\Database\Eloquent\Model;

class Merge extends Model
{
    protected $table = 'merges';

    protected $fillable = [
        'user_id',
        'owner_id',
        'merge_request_id',
        'from'
    ];

    public function moderator()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}