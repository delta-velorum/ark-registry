<?php


namespace App\Models;


use App\User;
use Illuminate\Database\Eloquent\Model;

class MergeRequest extends Model
{
    protected $table = 'merge_requests';

    protected $fillable = [
        'user_id',
        'owner_id',
        'model',
        'payload'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function merge()
    {
        return $this->hasOne(Merge::class);
    }
}