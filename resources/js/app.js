import Vue from 'vue';
import router from '~/router'
import {BootstrapVue} from 'bootstrap-vue';
import store from '~/store';

import Layout from './Layout.vue'
import Editor from 'v-markdown-editor';

Vue.use(BootstrapVue);
Vue.use(Editor);

Vue.component('navbar', require('./components/Navbar').default);
Vue.component('bottom', require('./components/Footer').default);

const app = new Vue({
    el: '#app',
    template: `<layout></layout>`,
    components: { Layout },
    router,
    store,
});
