/*
* Created by Michel3951
* Date: 3/22/2020 at 10:18 AM
* Want your own javascript script, or found an issue with this one.
* contact me on Discord (Michel3951#6705) or at
* https://michel3951.com
*/

import Toast from 'izitoast';
import axios from 'axios';
import store from '~/store';
import router from '~/router';
import config from '~/config';

axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
axios.defaults.headers.common['Accept'] = 'application/json';
axios.defaults.baseURL = config.apiURL;
axios.defaults.withCredentials = true;

// Request interceptor
axios.interceptors.request.use(async request => {
    // const token = store.getters['auth/token'];
    // if (token) {
    //     request.headers.common['Authorization'] = `Bearer ${token}`;
    // }

    return request;
});

// Response interceptor
axios.interceptors.response.use(response => response, error => {
    const { status } = error.response;

    if (status >= 500) {
        Toast.show({
            title: 'Something went wrong...',
            message: 'r/woooosh. Contact support if this issue persists.',
            color: 'red',
        });
    }

    // if (status === 404) {
    //     router.push('/404');
    // }

    if (status === 429) {
        Toast.show({
            title: 'HEY! Slow down!',
            message: 'You are making too many requests. Wait for a bit and try again.',
            color: 'red',
        });
    }

    // if (status === 401 && store.getters['auth/check']) {
    //     store.commit('auth/logout');
    //
    //     router.push({ name: 'auth/login' });
    // }

    return Promise.reject(error);
});

export default axios;
