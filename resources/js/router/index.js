import Vue from 'vue';
import VueRouter from 'vue-router';

import Home from '~/views/Home.vue';
import NotFound from '~/views/404.vue';

import store from "~/store";

// routes

import categoryRoutes from "./routes/categories";
import authRoutes from "./routes/auth";

// middleware
import guest from "./middleware/guest"
import auth from "./middleware/auth"

Vue.use(VueRouter);

const router = new VueRouter({
    mode: 'history',
    linkExactActiveClass: 'is-active',
    routes: [
        {
            path: '/',
            component: Home,
            name: 'home',
        },

        ...categoryRoutes,

        ...authRoutes,

        {
            path: '*',
            component: NotFound,
            name: 'errors/not-found',
        },
    ],
});

router.beforeEach((to, from, next) => {
    if (!to.meta.middleware) {
        return next()
    }

    const middleware = to.meta.middleware;

    const context = {
        to,
        from,
        next,
        store
    };

    return middleware[0]({
        ...context,
    })
});

export default router;
