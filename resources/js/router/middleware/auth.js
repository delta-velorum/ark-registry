export default async function auth({next, store}) {

    await store.dispatch('auth/fetchUser');

    if (!store.getters['auth/user']) {
        return next({
            name: 'auth/login'
        });
    }

    return next()
}
