export default async function guest({next, store}) {

    await store.dispatch('auth/fetchUser');

    if (store.getters['auth/user']) {
        return next({
            name: 'home'
        });
    }

    return next()
}
