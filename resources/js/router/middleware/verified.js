export default async function verified({next, store}) {

    await store.dispatch('auth/fetchUser');

    if (!store.getters['auth/user']) {
        return next({
            name: 'auth/login'
        });
    }

    if (!store.getters['auth/user'].email_verified_at) {
        return next({
            name: 'auth/verify'
        });
    }

    return next()
}
