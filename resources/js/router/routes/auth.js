import Login from '~/views/auth/Login';
import Forgot from '~/views/auth/Forgot';
import Reset from '~/views/auth/Reset';
import Verify from '~/views/auth/Verify';

import guest from "../middleware/guest"
import auth from "../middleware/auth"

const authRoutes = [
    {
        path: '/login',
        component: Login,
        name: 'auth/login',
        meta: {
            middleware: [
                guest
            ]
        },
    },
    {
        path: '/password/request',
        component: Forgot,
        name: 'auth/request',
    },
    {
        path: '/password/reset/:token',
        component: Reset,
        name: 'auth/reset',
    },
    {
        path: '/email/verify',
        component: Verify,
        name: 'auth/verify',
        meta: {
            middleware: [
                auth
            ]
        }
    }
];

export default authRoutes;
