import Create from '~/views/categories/Create';
import Update from '~/views/categories/Update';
import auth from '../middleware/auth';
import verified from '../middleware/verified';

const categoryRoutes = [
    {
        path: '/categories/new',
        component: Create,
        name: 'categories/new',
        meta: {
            middleware: [
                verified
            ]
        }
    },
    {
        path: '/categories/:slug/edit',
        component: Update,
        name: 'categories/edit',
        meta: {
            middleware: [
                verified
            ]
        }
    }
];

export default categoryRoutes;
