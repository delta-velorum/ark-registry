import axios from 'axios';
import * as types from '../mutations';

// state
export const state = {
    user: null,
};

// getters
export const getters = {
    user: state => state.user,
};

// mutations
export const mutations = {
    [types.FETCH_USER_SUCCESS](state, {user}) {
        state.user = user;
    },

    [types.FETCH_USER_FAILURE](state) {
        state.user = null;
    },

    [types.LOGOUT](state) {
        state.user = null;
    },

    [types.UPDATE_USER](state, {user}) {
        state.user = user;
    },
};

// actions
export const actions = {
    async fetchUser({commit}) {
        try {
            const {data} = await axios.get('/auth/check');

            commit(types.FETCH_USER_SUCCESS, {user: data});
        } catch (e) {
            commit(types.FETCH_USER_FAILURE);
        }
    },

    updateUser({commit}, payload) {
        commit(types.UPDATE_USER, payload);
    },

    async logout({commit}) {
        try {
            await axios.post('/logout');
        } catch (e) {
        }

        commit(types.LOGOUT);
    },
};
