window.Vue = require('vue');

Vue.component('merges', require('./components/Merges/Index.vue').default);
Vue.component('metadata', require('./components/MetaData.vue').default);

let app = new Vue({
    el: '#app'
});