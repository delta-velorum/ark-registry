@extends('layouts.app')

@section('content')
    <div class="container-fluid px-lg-5">
        <div class="card shadow">
            <div class="card-body">
                <form action="{{ route('categories.store') }}" method="POST">
                    @csrf

                    <div class="form-group row">
                        <div class="col-lg">
                            <label for="name">Name <abbr title="Required">*</abbr></label>
                            <input type="text" name="name" id="name" class="form-control @error('name') is-invalid @enderror" value="{{ old('name') }}">
                            @error('name')
                            <span class="invalid-feedback" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="col-xl">
                            <label for="slug">Slug <abbr title="Required">*</abbr></label>
                            <div class="input-group @error('slug') is-invalid @enderror">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">{{ request()->root() }}/categories/</span>
                                </div>
                                <input type="text" name="slug" id="slug" class="form-control" value="{{ old('slug') }}">
                                <div class="input-group-append">
                                    <button class="btn btn-secondary" type="button" onclick="$('#slug').val($('#name').val().toLowerCase().replace(/[^0-9a-zA-Z-\s]/g, '').replace(/\s+/g, '-'))">Generate based on name</button>
                                </div>
                            </div>
                            @error('slug')
                            <span class="invalid-feedback" role="alert">{{ $message }}</span>
                            @enderror
                            <small class="text-muted">Only alphanumeric characters and dashes are allowed.</small>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="editor">Description</label>
                        <textarea id="editor" name="description" class="@error('slug') is-invalid @enderror">{{ old('description') }}</textarea>
                        @error('description')
                        <span class="invalid-feedback" role="alert">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary float-right">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop

@push('js')
    <script>
        let editor = new SimpleMDE({
            element: document.getElementById('editor')
        })
    </script>
@endpush