@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Categories</h1>
        <hr>
        <table class="table table-borderless table-striped">
            <thead class="thead-dark">
            <tr>
                <th>Name</th>
                <th>Items / Recipes</th>
                <th>Latest change</th>
                @if (Auth::check())
                    <th>Actions</th>
                @endif
            </tr>
            </thead>
            <tbody>
            @foreach($categories as $category)
                <tr>
                    <th>{{ $category->name }}</th>
                    <td>0</td>
                    <td>Nothing</td>
                    @if (Auth::check())
                        <td>
                            <a href="{{ route('categories.edit', $category->slug) }}" class="text-warning">Edit</a>
                        </td>
                    @endif
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@stop