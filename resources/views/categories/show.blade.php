@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>{{ $category->name }}</h1>
        <div class="description">
            {!! Markdown::convertToHtml($category->description) !!}
        </div>
        <div class="row">
            <div class="col-md">
                <div class="card shadow-sm">
                    <div class="card-header">
                        <h3>Items in this category</h3>
                    </div>
                    <div class="card-body">

                    </div>
                </div>
            </div>
            <div class="col-md">
                <div class="card shadow-sm">
                    <div class="card-header">
                        <h3>Recipes in this category</h3>
                    </div>
                    <div class="card-body">

                    </div>
                </div>
            </div>
        </div>
    </div>
@stop