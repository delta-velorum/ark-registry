@extends('layouts.app')

@section('content')
    <div class="container-fluid px-lg-5">
        <div class="card shadow">
            <div class="card-body">
                <form action="{{ route('items.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf

                    <div class="form-group row">
                        <div class="col-lg">
                            <label for="name">Name <abbr title="Required">*</abbr></label>
                            <input type="text" name="name" id="name" class="form-control @error('name') is-invalid @enderror" value="{{ old('name') }}">
                            @error('name')
                            <span class="invalid-feedback" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="col-xl">
                            <label for="slug">Slug <abbr title="Required">*</abbr></label>
                            <div class="input-group @error('slug') is-invalid @enderror">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">{{ request()->root() }}/items/</span>
                                </div>
                                <input type="text" name="slug" id="slug" class="form-control" value="{{ old('slug') }}">
                                <div class="input-group-append">
                                    <button class="btn btn-secondary" type="button" onclick="$('#slug').val($('#name').val().toLowerCase().replace(/[^0-9a-zA-Z-\s]/g, '').replace(/\s+/g, '-'))">
                                        Generate based on name
                                    </button>
                                </div>
                            </div>
                            @error('slug')
                            <span class="invalid-feedback" role="alert">{{ $message }}</span>
                            @enderror
                            <small class="text-muted">Only alphanumeric characters and dashes are allowed.</small>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-6 col-lg-3 mb-2">
                            <label for="added_in">Added in <abbr title="Required">*</abbr></label>
                            <div class="input-group @error('added_in') is-invalid @enderror">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">V</span>
                                </div>
                                <input type="text" name="added_in" id="added_in" class="form-control" value="{{ old('added_in') }}" placeholder="297">
                            </div>
                            @error('added_in')
                            <span class="invalid-feedback" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="col-md-6 col-lg-3 mb-2">
                            <label for="removed_in">Removed in</label>
                            <div class="input-group @error('removed_in') is-invalid @enderror">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">V</span>
                                </div>
                                <input type="text" name="removed_in" id="removed_in" class="form-control" value="{{ old('removed_in') }}" placeholder="297">
                            </div>
                            @error('removed_in')
                            <span class="invalid-feedback" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="col-md-6 col-lg-3 mb-2">
                            <label for="weight">Weight <abbr title="Required">*</abbr></label>
                            <div class="input-group @error('weight') is-invalid @enderror">
                                <input type="number" step="0.1" name="weight" id="weight" class="form-control" value="{{ old('weight') }}">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">kg</span>
                                </div>
                            </div>
                            @error('weight')
                            <span class="invalid-feedback" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="col-md-6 col-lg-3 mb-2">
                            <label for="stack_size">Stack size <abbr title="Required">*</abbr></label>
                            <input type="number" name="stack_size" id="stack_size" class="form-control @error('stack_size') is-invalid @enderror" value="{{ old('stack_size') }}">
                            @error('stack_size')
                            <span class="invalid-feedback" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="editor">Description</label>
                        <textarea id="editor" name="description" class="@error('description') is-invalid @enderror">{{ old('description') }}</textarea>
                        @error('description')
                        <span class="invalid-feedback" role="alert">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="form-group row">
                        <div class="col-md-auto mb-3">
                            <label for="image_url">Image</label>
                            <input type="file" class="form-control-file @error('image_url') is-invalid @enderror" id="image_url" name="image_url">
                            @error('image_url')
                            <span class="invalid-feedback" role="alert">{{ $message }}</span>
                            @enderror
                        </div>

                        <div class="col-md">
                            <label for="dlc">DLC</label>
                            <select name="dlc" id="dlc" class="form-control @error('image_url') is-invalid @enderror">
                                <option value="">None</option>
                                @foreach ($dlcs as $dlc)
                                    <option value="{{ $dlc->id }}">{{ $dlc->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <metadata :old="{{ json_encode(old('metadata')) }}"></metadata>

                    <div class="form-group">
                        <button class="btn btn-primary float-right">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop

@push('js')
    <script>
        let editor = new SimpleMDE({
            element: document.getElementById('editor')
        })
    </script>
@endpush