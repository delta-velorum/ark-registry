@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Items</h1>
        <hr>
        <div class="card shadow">
            <div class="table-responsive">
                <table class="table table-borderless table-striped">
                    <thead class="thead-dark">
                    <tr>
                        <th>Name</th>
                        <th>Items / Recipes</th>
                        <th>Latest change</th>
                        @if (Auth::check())
                            <th>Actions</th>
                        @endif
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($items as $item)
                        <tr>
                            <td><img src="{{ asset($item->image_url) }}" alt="ark {{ $item->name }}" width="32"></td>
                            <td>{{ $item->name }}</td>
                            <td>0</td>
                            <td>Nothing</td>
                            @if (Auth::check())
                                <td>
{{--                                    <a href="{{ route('item.edit', $item->slug) }}" class="text-warning">Edit</a>--}}
                                </td>
                            @endif
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@stop