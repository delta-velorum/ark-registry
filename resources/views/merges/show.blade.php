@extends('layouts.app')

@section('content')
    <div class="container-fluid px-lg-5">
        <h1>Merge Request #{{ $request->id }}</h1>
        <div class="row">
            <div class="col-lg-4">
                <ul class="list-unstyled">
                    <li><b>Type</b>: {{ $request->model }}</li>
                    <li><b>Foreign owner key</b>: {{ $request->owner_id }}</li>
                    <li><b>User</b>: {{ $request->user->name }}</li>
                </ul>
            </div>
            <div class="col-lg-6">
                @if (!!$request->merge)
                    <ul class="list-unstyled">
                        <li><b>Status</b>: <span class="badge badge-success">Completed</span></li>
                        <li><b>Merged by</b>: {{ $request->merge->moderator->name }}</li>
                        <li>
                            <b>Merged at</b>:
                            {{ \Carbon\Carbon::parse($request->merge->created_at)->format('F j Y') }}
                            ({{ \Carbon\Carbon::parse($request->merge->created_at)->diffForHumans() }})
                        </li>
                    </ul>
                @else
                    <ul class="list-unstyled">
                        <li><b>Status</b>: <span class="badge badge-warning">Open</span></li>
                        <li>
                            This merge request is open, please review the data carefully and merge if it is correct.
                        </li>
                    </ul>
                @endif
            </div>
            <div class="col-lg-2">
                @if (!!!$request->merge)
                    <form action="{{ route('merges.approve', $request->id) }}" method="POST">
                        @csrf
                        <button class="btn btn-success w-100 mb-3">Approve</button>
                    </form>
                    <form action="{{ route('merges.delete', $request->id) }}" method="POST">
                        @csrf
                        <button class="btn btn-danger w-100">Delete</button>
                    </form>
                @else
                    <form action="{{ route('merges.revert', $request->id) }}" method="POST">
                        @csrf
                        <button class="btn btn-danger w-100">Undo changes</button>
                    </form>
                @endif
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-xl-5 mb-3">
                <div class="card shadow">
                    <div class="card-header">
                        <h1>From</h1>
                    </div>
                    <div class="card-body">
                        <ul class="list-unstyled">
                            @foreach($keys as $key)
                                @if ($from)
                                    @if (!in_array($key, ['created_at', 'updated_at', 'id']))
                                        <li><b>{{ $key }}</b>: {{ $from->{$key} }}</li>
                                    @endif
                                @else
                                    @if (!in_array($key, ['created_at', 'updated_at', 'id']))
                                        <li><b>{{ $key }}</b>: {{ $original->{$key} }}</li>
                                    @endif
                                @endif
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-xl-2 mb-3 d-flex mh-100">
                <div class="m-auto">
                    <i class="fas fa-arrow-right d-none d-xl-block" style="font-size: 3rem"></i>
                    <i class="fas fa-arrow-down d-xl-none" style="font-size: 3rem"></i>
                </div>
            </div>
            <div class="col-xl-5">
                <div class="card shadow">
                    <div class="card-header">
                        <h1>To</h1>
                    </div>
                    <div class="card-body">
                        <ul class="list-unstyled">
                            @foreach($keys as $key)
                                @if (!in_array($key, ['created_at', 'updated_at', 'id']))
                                    <li><b>{{ $key }}</b>: {{ $request->payload->{$key} ?? $original->{$key} }}</li>
                                @endif
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop