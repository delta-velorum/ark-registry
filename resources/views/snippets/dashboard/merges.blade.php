@foreach($requests as $request)
    <tr>
        <th>{{ $request->id }}</th>
        <td>{{ $request->user->name }}</td>
        <td>{{ $request->model }}</td>
        <td>
            @if (!!$request->completed)
                <i class="fas fa-check text-success"></i>
            @else
                <i class="fas fa-times text-danger"></i>
            @endif
        </td>
    </tr>
@endforeach