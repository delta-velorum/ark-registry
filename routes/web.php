<?php

use Illuminate\Support\Facades\Route;

Route::group([
    'namespace' => 'Auth'
], function () {
    Route::get('/api/v1/auth/check', 'UserController@show');
    Route::get('email/verify', 'VerificationController@show')->name('verification.notice');
    Route::get('email/verify/{id}', 'VerificationController@verify')->name('verification.verify');
    Route::patch('email/resend', 'VerificationController@resend')->name('verification.resend');
});

Route::group([
    'prefix' => '/api/v1',
], function() {
    Route::group([
        'prefix' => '/categories',
        'namespace' => 'Categories',
    ], function () {

        Route::get('/{category:slug}', 'CategoryController@show')->name('categories.show');

        Route::group([
            'middleware' => [
                'auth', 'verified'
            ]
        ], function () {
            Route::post('/new', 'ManageCategoryController@store')->name('categories.store');
            Route::patch('/{category:slug}/edit', 'ManageCategoryController@update')->name('categories.update');
        });
    });
});



Route::get('/{any}', 'HomeController@index')->where('any', '.*');

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::group([
    'prefix' => '/items',
    'namespace' => 'Items',
], function () {

    Route::group([
        'middleware' => [
            'auth'
        ]
    ], function () {
        Route::get('/add', 'ManageItemController@create')->name('items.create');
        Route::post('/add', 'ManageItemController@store')->name('items.store');
        Route::get('/{slug:slug}/edit', 'ManageItemController@edit')->name('items.edit');
        Route::post('/{slug:slug}/edit', 'ManageItemController@update')->name('items.update');
    });

    Route::get('/', 'ItemController@index')->name('items');
    Route::get('/{slug:slug}', 'ItemController@show')->name('items.show');
});

Route::group([
    'prefix' => '/dashboard/merges',
    'namespace' => 'Merges',
], function () {

    Route::group([
        'middleware' => [
            'auth',
            'moderator'
        ]
    ], function () {
        Route::get('/', 'MergeController@index')->name('merges.index');
        Route::get('/json', 'ManageMergeController@json')->name('merges.json');
        Route::get('/html', 'ManageMergeController@html')->name('merges.html');
        Route::post('/{id}/approve', 'ManageMergeController@approve')->name('merges.approve');
        Route::post('/{id}/delete', 'ManageMergeController@delete')->name('merges.delete');
        Route::post('/{id}/revert', 'ManageMergeController@revert')->name('merges.revert');
        Route::get('/{id}', 'MergeController@show')->name('merges.show');
    });
});


